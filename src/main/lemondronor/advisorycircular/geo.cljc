;; Most of this was written before we started using geolib. Maybe
;; replace with geolib functions?

(ns lemondronor.advisorycircular.geo
  (:require
   [lemondronor.advisorycircular.logging :as logging]
   ["geolib" :as geolib]))

(declare logger log-debug log-verbose log-info log-warn log-error)
(logging/deflog "geo" logger)


(def to-radians
  #?(:clj Math/toRadians
     :cljs (fn [d] (/ (* d Math/PI) 180))))


(def to-degrees
  #?(:clj Math/toDegrees
     :cljs (fn [d] (/ (* d 180) Math/PI))))


(defn centroid [coords]
  (let [center (js->clj (geolib/getCenter (clj->js coords)) :keywordize-keys true)
        xcenter {:lat (:latitude center)
                 :lon (:longitude center)}]
    xcenter))


(defn distance
  "Returns the distance between two positions, in km."
  [pos1 pos2]
  (let [earth-radius 6372.8 ;; km
        sin2 (fn sin2 ^double [^double theta] (* (Math/sin theta) (Math/sin theta)))
        alpha (fn alpha ^double [^double lat1 ^double lat2 ^double delta-lat ^double delta-lon]
                (+ (sin2 (/ delta-lat 2.0))
                   (* (sin2 (/ delta-lon 2)) (Math/cos lat1) (Math/cos lat2))))
        {lat1 :lat lon1 :lon} pos1
        {lat2 :lat lon2 :lon} pos2
        delta-lat (to-radians (- ^double lat2 ^double lat1))
        delta-lon (to-radians (- ^double lon2 ^double lon1))
        lat1 (to-radians lat1)
        lat2 (to-radians lat2)]
    (* earth-radius 2
       (Math/asin (Math/sqrt (alpha lat1 lat2 delta-lat delta-lon))))))


(defn position= [p1 p2]
  (= p1 p2))


(defn bearing
  "Returns the bearing from one position to another, in degrees."
  [pos1 pos2]
  (if (position= pos1 pos2)
    nil
    (let [{lat1 :lat lon1 :lon} pos1
          {lat2 :lat lon2 :lon} pos2
          lat1 ^double (to-radians lat1)
          lat2 ^double (to-radians lat2)
          lon-diff ^double (to-radians (- ^double lon2 ^double lon1))
          y ^double (* (Math/sin lon-diff) (Math/cos lat2))
          x ^double (- (* (Math/cos lat1) (Math/sin lat2))
                       (* (Math/sin lat1) (Math/cos lat2) (Math/cos lon-diff)))]
      (mod (+ (to-degrees (Math/atan2 y x)) 360.0) 360.0))))


(defn bearing-diff
  "Computes difference between two bearings. Result is [-180, 180]."
  ^double [a b]
  (let [d (- 180.0 (mod (+ (- a b) 180.0) 360.0))]
    d))


(defn spurious-bearing [bearing]
  (nil? bearing))


(defn spurious-bearing-diff [^double bearing-diff]
  (> (Math/abs bearing-diff) 160))


(defn curviness
  "Computes the total curvature of a sequence of bearings."
  ^double [bearings]
  (Math/abs
   ^double (reduce (fn [^double sum [^double a ^double b]]
                     (let [d (bearing-diff a b)]
                       (if (spurious-bearing-diff d)
                         sum
                         (+ sum d))))
                   0.0
                   (partition 2 1 bearings))))


;; flight is a vector of {:lat <lat> :lon <lon}.

(defn flight-curviness [flight]
  (->> (partition 2 1 flight)
       (map #(apply bearing %))
       (filter #(not (spurious-bearing %)))
       curviness))


(defn flight-distance [flight]
  (->> (partition 2 1 flight)
       (map #(apply distance %))
       (apply +)))


(defn flight-normalized-curviness [flight]
  (/ (flight-curviness flight)
     (flight-distance flight)))
