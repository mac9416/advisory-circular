(ns lemondronor.advisorycircular.logging
  (:require
   [goog.string :as gstring]
   goog.string.format
   ["winston" :as winston])
  (:require-macros
   [lemondronor.advisorycircular.logging]))


;; (def format-msg [msg]
;;   (gstring/format "%s%-7s %-9s/%-18s| %s"
;;                                   (.-timestamp %)
;;                                   (.-ms %)
;;                                   (.-service %)
;;                                   (.-level %)
;;                                   (.-message %)))

(def log-prefix (atom ""))

(defn set-log-prefix! [prefix]
  (swap! log-prefix (constantly prefix)))


(defn format-msg [msg]
  (let [prefix (if (and @log-prefix (> (count @log-prefix) 0))
                 (str @log-prefix " ")
                 "")]
    (gstring/format "%s%s %s"
                    prefix
                    (.-level msg)
                    (.-message msg))))


(let [createLogger (.-createLogger winston)
      format (.-format winston)
      transports (.-transports winston)]
  (def logger (createLogger
               #js {:level "info"
                    :format (.combine
                             format
                             (.colorize format #js {:all true})
                             (.timestamp format #js {:format "YYYYMMDD HHmmss"})
                             (.errors format #js {:stack true})
                             (.splat format)
                             (.timestamp format)
                             (.label format)
                             (.ms format)
                             (.json format))
                    :defaultMeta #js {}}))
  (.add logger (new (.-Console transports)
                    #js {:format (.combine format
                                           (.printf format format-msg))})))


(defn get-logger [service]
  (.child logger #js {:service service}))
