(ns lemondronor.advisorycircular.util-test
  (:require [cljs.test :refer (deftest is testing)]
            [lemondronor.advisorycircular.util :as util]))

(deftest deep-merge
  (is (= (util/deep-merge
          {:adsbx {:url "http://bar"}}
          {:adsbx {:url "http://foo"}}
          {:adsbx {:secret "123"}})
         {:adsbx {:url "http://foo", :secret "123"}}))
  (is (= (util/deep-merge
          {:adsbx {:url "http://bar"}}
          {}
          {:adsbx {:secret "123"}})
         {:adsbx {:url "http://bar", :secret "123"}})))


(deftest nested-keys
  (is (= (util/nested-keys {:a 1 :b 2})
         [[:a] [:b]]))
  (is (= (util/nested-keys {:a {:b {:c 1} :c {:d 2}} :b {:c 3} :c 4})
         [[:a :b :c] [:a :c :d] [:b :c] [:c]])))
